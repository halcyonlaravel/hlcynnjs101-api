# Adonis Slim App

The Adonis slim app is the tinest boilerplate to create Adonisjs applications with minimal footprint and you get all the goodies of Adonis IoC container, autoloading, ace commands etc.

# Setup
```
git clone git@bitbucket.org:halcyonlaravel/adonisjs-slim-basics.git
cd adonisjs-slim-basics/
npm install
cp .env.example .env 
```

## What's next?

This project structure can scale as you go, simply execute the `ace` commands to create **Controllers**, **Models**, etc for you. 

Also make sure to read the [guides](http://dev.adonisjs.com/docs/4.1/installation)

