"use strict";

const Article = use("App/Models/Article");
const { sanitizor } = use("Validator");

class ArticleController {
  /**
   * @swagger
   *
   * /login:
   *   post:
   *     description: Login to the application
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: username
   *         description: Username to use for login.
   *         in: formData
   *         required: true
   *         type: string
   *       - name: password
   *         description: User's password.
   *         in: formData
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: login
   */
  async index({ response }) {
    const articles = await Article.find({}).lean();

    return response.ok({ message: "success", data: articles });
  }

  async show({ params, response }) {
    const { slug } = params;

    const article = await Article.findOne({ slug }).lean();

    return response.ok({ message: "success", data: article });
  }

  async store({ request, response }) {
    const { title, body } = request.post();

    const article = await Article.create({
      title,
      slug: sanitizor.slug(title),
      body
    });

    return response.created({ message: "created", data: article });
  }

  async update({ request, response, params }) {
    const { title, body } = request.all();
    const { slug } = params;

    const article = await Article.findOne({ slug: slug });
    article.title = title;
    article.slug = sanitizor.slug(title);
    article.body = body;
    article.updatedAt = new Date();

    const updatedArticle = await article.save();

    return response.ok({ message: "updated", data: updatedArticle });
  }

  async destroy({ params, response }) {
    const { slug } = params;

    const article = await Article.findOne({ slug: slug });
    await article.remove();

    return response.ok({ message: "deleted" });
  }
}

module.exports = ArticleController;
