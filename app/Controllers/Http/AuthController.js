// /app/Controllers/Http/AuthController

const Jwt = use("Jwt");
const Hash = use("Hash");
const User = use("App/Models/User");

class AuthController {
  async login({ request, response }) {
    const { email, password } = request.post();

    const user = await User.findOne(
      { email, deletedAt: null },
      "+password"
    ).lean();

    if (!user) {
      return response
        .status(422)
        .json({ message: "Invalid Username or Password" });
    }

    const isSame = await Hash.verify(password, user.password);

    if (!isSame) {
      return response
        .status(422)
        .json({ message: "Invalid Username or Password" });
    }

    const token = Jwt.generate({
      user: String(user._id)
    });

    return response.ok({ message: "Login Successful", data: token });
  }

  async register({ request, response }) {
    const { email, firstName, lastName, password } = request.post();

    try {
      const user = await User.create({
        email,
        name: { firstName, lastName },
        password: await Hash.make(password)
      });

      return response
        .status(201)
        .json({ data: user, message: "Registration Successful" });
    } catch (error) {
      console.log(error);
      return response.status(500).json({ message: "Something went wrong" });
    }
  }

  async user({ response, auth }) {
    return response.ok({ data: auth.user });
  }
}

module.exports = AuthController;
