"use strict";

const Mongoose = use("Mongoose");
const { Schema } = use("Mongoose");

const ArticleSchema = new Schema({
  title: { type: String, required: true },
  slug: {
    type: String,
    unique: true,
    index: true,
    required: true
  },
  body: { type: String, default: "" },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  createdAt: { type: Date, default: Date.now }
});

module.exports = Mongoose.model("Article", ArticleSchema);
