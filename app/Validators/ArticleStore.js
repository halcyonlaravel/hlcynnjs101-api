"use strict";

const CustomValidators = use("App/Validators/CustomValidators");

class ArticleStore extends CustomValidators {
  get sanitizationRules() {
    return {
      title: "trim|escape",
      body: "trim|escape"
    };
  }

  get rules() {
    return {
      title: "required|uniqueDocumentField:articles,title",
      body: "required"
    };
  }

  get messages() {
    return {
      "title.required": "Title field is required",
      "title.uniqueDocumentField": "Title must be unique",
      "body.required": "Body field is required"
    };
  }

  get validateAll() {
    return true;
  }

  get data() {
    const data = this.ctx.request.all();

    return data;
  }

  async fails(errorMessages) {
    return this.ctx.response
      .status(422)
      .json({ message: "Validation Failed", errors: errorMessages });
  }
}

module.exports = ArticleStore;
