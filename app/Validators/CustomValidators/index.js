'use strict';

const { forEach } = require('lodash');
const Validator = use('Validator');

class CustomValidators {
  constructor() {
    const customValidators = { ...require('require-all')(`${__dirname}/lib`) };
    forEach(customValidators, function(value, key) {
      Validator.extend(key, value);
    });
  }
}

module.exports = CustomValidators;
