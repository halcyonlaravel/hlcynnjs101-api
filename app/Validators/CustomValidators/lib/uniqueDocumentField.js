const Mongoose = use("Mongoose");

module.exports = async (data, field, message, args, get) => {
  const value = get(data, field);

  if (!value) return;

  const [collection, documentField] = args;

  const conditions = {};

  conditions[documentField] = value;

  const document = await Mongoose.connection.db
    .collection(collection)
    .findOne(conditions);

  if (document) throw message;
};
