const Route = use("Route");
const Env = use('Env');
const Config = use("Config");
const swaggerJSDoc = use('swagger-jsdoc');

// Articles
Route.get("/articles", "ArticleController.index");
Route.post("/articles", "ArticleController.store").validator("ArticleStore");
Route.get("/articles/:slug", "ArticleController.show");
Route.put("/articles/:slug", "ArticleController.update").validator(
  "ArticleStore"
);
Route.delete("/articles/:slug", "ArticleController.destroy");

// Auth
Route.post("/register", "AuthController.register");
Route.post("/login", "AuthController.login");
Route.get("/user", "AuthController.user").middleware("auth");


// Route.get('/api-specification', () => {
//   return `
// openapi: "3.0.0"
// info:
//   version: 1.0.0
//   title: API Documentation
//   license:
//     name: MIT
// servers:
//   - url: ${Env.get('APP_URL')}
// components:
//   schemas:
//     Article:
//       type: object
//       properties:
//         title:
//           type: string
//         slug:
//           type: string
//         body:
//           type: string
// paths:
//   /articles:
//     get:
//       tags:
//         - Articles
//       summary: List all articles
//       operationId: listArticles
//       responses:
//         200:
//           description: Article list
//           content:
//             application/json:
//               schema:
//                 type: object
//                 properties:
//                   message:
//                     type: string
//                   data:
//                     type: array
//                     items:
//                       $ref: "#/components/schemas/Article"
//     post:
//       summary: Create an article
//       operationId: createArticle
//       tags:
//         - Articles
//       requestBody:
//         content:
//           multipart/form-data:
//             schema:
//               type: object
//               required:
//                 - title
//                 - body
//               properties:
//                 title:
//                   type: string
//                   default: Default Title
//                 body:
//                   type: string
//                 slug:
//                   type: string
//       responses:
//         201:
//           description: Article created
//           content:
//             application/json:
//               schema:
//                 $ref: "#/components/schemas/Article"
// `
// })


Route.get('/api-specification', async () => {
  // https://swagger.io/docs/specification/about/
  const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: Config.get('app.name'),
      version: '1.0.0'
    },
    servers: [{ url: `/` }],
    basePath: '/',
    security: [{ bearerAuth: [] }],
    schemes: Env.get('NODE_ENV') === 'production' ? ['https'] : ['http'],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    }
  };

  const options = {
    definition: swaggerDefinition,
    apis: ['./app/**/**.js', './start/**/**.yaml']
  };

  return swaggerJSDoc(options);
});

Route.get('/api-documentation', ({ view }) => {
  return view.render('swagger', { specUrl: '/api-specification' })
})

Route.get('/chat', ({ view }) => {
  return view.render('chat')
})
