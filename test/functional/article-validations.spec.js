"use strict";

const { test, trait, beforeEach } = use("Test/Suite")("Article");
const Article = use("App/Models/Article");
const chancejs = require("chance").Chance();

trait("Test/ApiClient");

beforeEach(async () => {
  await Article.deleteMany({});
});

test("article title must be unique", async ({ client }) => {
  const article = {
    title: "Adonis 101",
    slug: "adonis-101",
    body: "Post content"
  };

  await Article.create(article);

  const response = await client
    .post("/articles")
    .send(article)
    .end();

  response.assertStatus(422);
  response.assertJSONSubset({
    message: "Validation Failed",
    errors: [{ field: "title", message: "Title must be unique" }]
  });
});
