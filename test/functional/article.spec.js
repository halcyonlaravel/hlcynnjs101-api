"use strict";

const { test, trait, beforeEach } = use("Test/Suite")("Article");
const Article = use("App/Models/Article");
const chancejs = require("chance").Chance();

trait("Test/ApiClient");

beforeEach(async () => {
  await Article.deleteMany({});
});

test("can list articles", async ({ assert, client }) => {
  for (let index = 0; index < 5; index++) {
    await Article.create({
      title: `Title ${index}`,
      slug: `title-${index}`,
      body: chancejs.paragraph()
    });
  }

  const response = await client.get("/articles").end();

  response.assertStatus(200);
  assert.equal(
    response.body.data.length,
    5,
    "articles listed does not match expected count"
  );
});

test("can create article", async ({ assert, client }) => {
  const article = {
    title: "Adonis 101",
    slug: "adonis-101",
    body: "Post content"
  };

  const response = await client
    .post("/articles")
    .send(article)
    .end();

  response.assertStatus(201);
  response.assertJSONSubset({
    data: article
  });
});

test("can update article", async ({ assert, client }) => {
  const article = {
    title: "Adonis 101",
    slug: "adonis-101",
    body: "Post content"
  };

  await Article.create(article);

  const updatedArticle = {
    title: "Adonis 202",
    slug: "adonis-202",
    body: "Post 202"
  };

  const response = await client
    .put(`/articles/${article.slug}`)
    .send(updatedArticle)
    .end();

  response.assertStatus(200);
  response.assertJSONSubset({
    data: updatedArticle
  });
});

test("can delete an article", async ({ assert, client }) => {
  const article = {
    title: "Adonis 101",
    slug: "adonis-101",
    body: "Post content"
  };

  await Article.create(article);

  const response = await client.delete(`/articles/${article.slug}`).end();

  response.assertStatus(200);
  response.assertJSONSubset({
    message: "deleted"
  });
});
